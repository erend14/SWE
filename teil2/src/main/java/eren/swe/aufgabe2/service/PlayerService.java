package eren.swe.aufgabe2.service;

import eren.swe.aufgabe2.repository.PlayerRepository;
import eren.swe.aufgabe2.model.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlayerService {


    @Autowired
    PlayerRepository playerRepository;

    public Player addPlayer(Player player) {
        return playerRepository.save(player);
    }

    public List<Player> getAllPlayers() {
        List<Player> players = new ArrayList<>();
        // players::add == players.add(player)
        playerRepository.findAll().forEach(players::add);
        return players;
    }

    public Player getOne(Integer id) {
       return playerRepository.findOne(id);
    }

    public Object findOne(Integer id) {
        return playerRepository.findOne(id);
    }
}
