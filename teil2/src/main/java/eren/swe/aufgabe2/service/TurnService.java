package eren.swe.aufgabe2.service;

import eren.swe.aufgabe2.model.Turn;
import eren.swe.aufgabe2.repository.TurnRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TurnService {

    @Autowired
    TurnRepository turnRepository;

    public void addTurn(Turn turn) {
        turnRepository.save(turn);
    }
}
