package eren.swe.aufgabe2.service;

import eren.swe.aufgabe2.model.Map;
import eren.swe.aufgabe2.repository.MapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MapService {

    @Autowired
    MapRepository mapRepository;

    public void save(Map gameMap) {
        mapRepository.save(gameMap);
    }
}
