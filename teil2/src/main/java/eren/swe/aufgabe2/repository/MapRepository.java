package eren.swe.aufgabe2.repository;

import eren.swe.aufgabe2.model.Map;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MapRepository extends CrudRepository<Map, Integer> {
}
