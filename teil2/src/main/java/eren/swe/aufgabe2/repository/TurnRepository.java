package eren.swe.aufgabe2.repository;

import eren.swe.aufgabe2.model.Turn;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TurnRepository extends CrudRepository<Turn, Integer>{
}
