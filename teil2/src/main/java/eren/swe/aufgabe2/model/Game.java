package eren.swe.aufgabe2.model;



import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="game")
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;




    private String gameScore;
    private String winner;
    private Integer maxNumberOfRounds;
    private Integer currentRound;


    private boolean isStarted;
    private boolean isFinished;

    public Game(){}

    @OneToMany(mappedBy = "game")
    private List<Turn> turns;

    @ManyToOne
    @JoinColumn(name = "player_1")
    private Player player1;
    private String player1Color;

    @ManyToOne
    @JoinColumn(name = "player_2")
    private Player player2;
    private String player2Color;

    @OneToOne(cascade = CascadeType.ALL)
    private Map map;


    private boolean isfull;
    private boolean canStart;


    public Game(Player asdl){
        init();
    }

    public boolean isCanStart() {
        return canStart;
    }

    public void setCanStart(boolean canStart) {
        this.canStart = canStart;
    }


    public String getGameScore() {
        return gameScore;
    }

    public void setGameScore(String gameScore) {
        this.gameScore = gameScore;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public Integer getMaxNumberOfRounds() {
        return maxNumberOfRounds;
    }

    public void setMaxNumberOfRounds(Integer maxNumberOfRounds) {
        this.maxNumberOfRounds = maxNumberOfRounds;
    }

    public Integer getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(Integer currentRound) {
        this.currentRound = currentRound;
    }

    public List<Turn> getTurns() {
        return turns;
    }

    public void setTurns(List<Turn> turns) {
        this.turns = turns;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public String getPlayer1Color() {
        return player1Color;
    }

    public void setPlayer1Color(String player1Color) {
        this.player1Color = player1Color;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public String getPlayer2Color() {
        return player2Color;
    }

    public void setPlayer2Color(String player2Color) {
        this.player2Color = player2Color;
    }

    public boolean isIsfull() {
        this.isfull = this.player1 != null && this.player2 != null;
        return isfull;
    }

    public void setIsfull(boolean isfull) {
        this.isfull = isfull;
    }


    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public Game(Player player1, String player1Color) {

        this.player1 = player1;
        this.player1Color = player1Color;
        init();
    }


    private void init(){
        this.canStart = false;
        this.isfull = false;
        this.maxNumberOfRounds = 200;
        this.currentRound = 0;
        this.winner = "none so far";
        this.map = null;
        this.player2 = null;
        this.player2Color = null;
        this.gameScore = "0:0";
        this.turns = new ArrayList<Turn>();

    }


    public Integer getId() {
        return id;
    }

  public boolean isStarted() {
        return isStarted;
    }

    public void setStarted(boolean started) {
        isStarted = started;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }   public void setId(Integer id) {
        this.id = id;
    }

    public Turn start() {

        if (this.map.getHalfmapAvalid() && this.map.getHalfmapBvalid()){
            this.setStarted(true);
            this.map.setEvalueated(true);
            this.currentRound = 1;
            Turn initialState = new Turn();
            initialState.setGame(this);
            initialState.setInventoryA(false);
            initialState.setInventoryB(false);
            initialState.setNewPositionA(this.map.getCoordinatesOfCastleB());
            initialState.setNewPositionB(this.map.getCoordinatesOfCastleA());
            initialState.setTurnNr(0);
            return initialState;

        }else {
            if (!this.map.getHalfmapBvalid() && this.map.getHalfmapAvalid()){
                this.setFinished(true);
                this.setWinner(this.player1.getNickname() + " has won!");
            }else if(!this.map.getHalfmapAvalid() && this.map.getHalfmapBvalid()) {
                this.setFinished(true);
                this.setWinner(this.player2.getNickname() + " has won!");
            }else {
                this.setFinished(true);
                this.setWinner("both players registered unvalid map. game is finished before it could start");
            }
            return null;
        }
    }

    public Turn addTurn(boolean isPlayerA, TurnHelper turn) throws Exception {
        Turn prevTurn = this.getTurns().stream().filter(t -> t.getTurnNr() == turn.getTurnNumber() -1 ).findAny().orElse(null);
        Turn currentTurn = this.getTurns().stream().filter(t -> t.getTurnNr() == turn.getTurnNumber()).findAny().orElse(null);
        Turn toReturn;
        if (prevTurn == null && currentTurn == null) throw new Exception("Invalid turn, it has not started yet");

        boolean carryingTreasure;
        boolean isMoveValid;
        boolean isOnWater;
        boolean isTreasureFound;
        boolean isOnMountain;
        boolean isInsideGameArea;

        isMoveValid = this.map.validateTurn(isPlayerA, turn);

        if (!isMoveValid) throw new Exception("move you are trying to make is not valid");

        toReturn =  this.addTurnForUser(isPlayerA,turn);

        this.currentRound++;
        if (this.currentRound == this.maxNumberOfRounds){
            this.setFinished(true);
        }
        return toReturn;
    }

    private Turn addTurnForUser(boolean isPlayerA, TurnHelper turn){


        Turn tT =   this.getTurns().stream().filter(t -> t.getTurnNr() == turn.getTurnNumber()).findFirst().orElse(null);
        boolean isNewGame= false;
        if (tT == null){
            tT = new Turn();
            isNewGame = true;
        }

        if (isPlayerA){
            // Player A
            tT.setTurnNr(turn.getTurnNumber());
            tT.setInventoryA(true); // TODO : do sth better
            tT.setOldPositionA(turn.getCurrentPosition());
            tT.setNewPositionA(turn.getNewPosition());
            tT.setGame(this);
            if(isNewGame) this.turns.add(tT);
            return tT;
        }else {
            // not player A
            tT.setTurnNr(turn.getTurnNumber());
            tT.setInventoryB(true); // TODO : do sth better
            tT.setOldPositionB(turn.getCurrentPosition());
            tT.setNewPositionB(turn.getNewPosition());
            tT.setGame(this);
            if(isNewGame) this.turns.add(tT);
            return tT;
        }

    }





}
