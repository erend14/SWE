# Business logic

- [X] Die Position des Avatars befindet sich auf der von der KI generierten Burg der Spielhälfte.
- [ ] Die gegnerische Burg und der eigene Schatz ist für die KIs nichtsichtbar.
- [ ] Das Spiel darf nicht länger als 200 Spielaktionen dauern.
- [X] Eine Burg und ein Schatz dürfen nicht auf dem gleichen Feld platziert werden.
- [X] KI sollten unter keinen Umständen die Terrainart Wasser betreten, ansonsten verlieren sie das Spiel.
- [X] Burgen und Schätze dürfen nur auf die Terrainart Wiese platziert werden.
- [X] Die Kartengrenze darf maximal drei Wasserfelder beinhalten.
- [X] Jede Kartenhälfte muss mindestens drei Bergfelder, fünf Wiesenfelder und vier Wasserfelder beinhalten.

---

# API REFERENCE


| Method                 | HTTP request                         | Description                                      |
| :--------------------- | :-------------------------           | :-----------------------------------------       |
| GetAllPlayers()        | GET  /api/player                     | Returns a list of all players as json            |
| GetPlayer(id)          | GET  /api/player/{id}                | Returns the player with {id} if exists           |
| GetGame(id)            | GET  /api/game/{id}                  | Returns the game   with {id} if exists           |
| GetAllGames()          | GET  /api/game                       | Returns a list of all Games                      |
| AddPlayer(player)      | POST /api/player                     | Adds the req. body as json                       |
| CreateNewGame()        | POST /api/player/{player_id}/create/ | Creates and returns a new game with req. body map|
| AddPlayerToGame()      | POST /api/game/{id}/join/{player_id} | adds player_id to game_id                        |
| DeletePlayer(id)       | DELETE  /api/player/{id}             | Deletes the player with {id} if exists           |

## Sample Requests
### GetAllPlayers()
```shell
curl --request GET \
  --url http://localhost:8080/api/player
```

### RegisterNewPlayer(Player)
```shell
curl --request POST \
  --url http://localhost:8080/api/player \
  --header 'content-type: application/json' \
  --data '{
	"firstname" : "john",
	"lastname": "doe",
	"matrNr" : "a1234567"
}'
```

### GetPlayer(id)
```shell
curl --request GET \
  --url http://localhost:8080/api/player/2
```

### GetAllGames()
```shell
curl --request GET \
  --url http://localhost:8080/api/game
```
### AddUserToGame(1,2)
```shell
curl --request POST \
  --url http://localhost:8080/api/game/2/join/1 \
  --header 'content-type: application/json' \
  --data '{
	"id":1
}'
```

 

---

# Test Cases

- [x] 1.  NetworkCheck getallplayers (get: /api/player)
- [x] 2.  Networkcheck register a new Player (post: /api/player)
- [x] 3.  Networkcheck delete a player (delete: /api/player/[:id])
- [ ] 4.  Networkcheck get a player (get: /api/player/[:id])
- [ ] 5.  Networkcheck getAllGames (get: /api/game)
- [ ] 6.  Networkcheck a getGame (get /api/game/[:id])
- [ ] 7.  Networkcheck create new game (post: /api/player/[:player_id]/create-game/)
- [ ] 8.  Networkcheck add player to an existing game(post: /api/game/[:game_id]/join/[:player_id]
- [ ] 9.  Networkcheck check if game is finished
- [ ] 10. Get the winner of the game

---